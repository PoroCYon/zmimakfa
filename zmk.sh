#!/usr/bin/env bash

case "$@" in
    "--help"|"-h"|"")
        BASE="$(basename "$0")"
        cat <<EOF
$BASE - execute a command whenever a file is written to.

Usage: $BASE <file> <command...>
    file:       The file to watch.
    command:    The command to execute. When the file is written to but the
                previous command is still running, the latter will be
                terminated.

Example:
    $BASE src/main.c "make && bin/a.out"
EOF
        exit
        ;;
    *)
        ;;
esac

FILE="$1"
shift
# program to exec now in $@

while true; do
    bash -c "$@" &
    BGID=$!
    inotifywait -e modify "$FILE" -qq
    # ignore errors - not really bad if the process has already stopped
    pkill -P "$BGID" >/dev/null
    pkill "$BGID" >/dev/null
done

