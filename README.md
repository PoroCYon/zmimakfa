# zmk

> zmiku/zmimakfa/...

Execute a command as soon as a file is written to.

## Usage

```
zmk - execute a command whenever a file is written to.

Usage: zmk <file> <command...>
    file:       The file to watch.
    command:    The command to execute. When the file is written to but the
                previous command is still running, the latter will be
                terminated.

Example:
    zmk src/main.c "make && bin/a.out"
```

## Dependencies

* `bash`
* `inotifywait`

